#!/bin/bash

# Clone the repository to /opt/ups-monitor and run this script to set it up for dashboard usage

if [ "$EUID" -ne 0 ]; then
  echo "Please run as root"
  exit
fi

SCRIPT_PATH="$(cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
OPT_PATH="/opt/ups-monitor"

if [ "$SCRIPT_PATH" != "$OPT_PATH" ]; then
  echo "please run script from inside $OPT_PATH directory"
  exit
fi

# NUT

if [[ "$(command -v upsc)" =~ "upsc" ]]; then
  echo "NUT is installed"
else
  apt update
  apt install nut -y
fi

# Python3

if [[ "$(command -v python3)" =~ "python3" ]]; then
  echo "Python3 is installed"
else
  apt update
  apt install python3 -y
fi

# Pip3

if [[ "$(command -v pip3)" =~ "pip3" ]]; then
  echo "Pip3 is installed"
else
  apt update
  apt install python3-pip -y
fi

# UPS configuration

UPS_DRIVER="$(sed -nr "/^\[hq-ups0\]/ { :l /^driver[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /etc/nut/ups.conf)"
if [ "$UPS_DRIVER" == "" ]; then
  if grep -Fxq "[hq-ups0]" /etc/nut/ups.conf; then
    sed -i 's/^\[hq-ups0\]$/\[hq-ups0\]\ndriver=usbhid-ups/' /etc/nut/ups.conf
  else
    echo "[hq-ups0]" >> /etc/nut/ups.conf
    echo "driver=usbhid-ups" >> /etc/nut/ups.conf
  fi
else
  sed -i '/^\[all\]$/,/^\[/ s/^driver=.*$/driver=usbhid-ups/' /etc/nut/ups.conf
fi

UPS_PORT="$(sed -nr "/^\[hq-ups0\]/ { :l /^port[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /etc/nut/ups.conf)"
if [ "$UPS_PORT" == "" ]; then
  if grep -Fxq "[hq-ups0]" /etc/nut/ups.conf; then
    sed -i 's/^\[hq-ups0\]$/\[hq-ups0\]\nport=auto/' /etc/nut/ups.conf
  else
    echo "[hq-ups0]" >> /etc/nut/ups.conf
    echo "port=auto" >> /etc/nut/ups.conf
  fi
else
  sed -i '/^\[all\]$/,/^\[/ s/^port=.*$/port=auto/' /etc/nut/ups.conf
fi

mkdir -p /var/run/nut
chown root:nut /var/run/nut
chmod 770 /var/run/nut

sed -ie "s/^\(#\|\)MODE=.*/MODE=standalone/" /etc/nut/nut.conf

# create monitor user

if id "monitor" > /dev/null 2>&1; then
  echo "monitor user exists"
else
  echo "Creating monitor user"
  adduser --disabled-password --gecos "" --quiet monitor 
fi

# MQTT Client

if [ ! -f "$OPT_PATH/lib/.env" ]; then
  echo "Creating empty .env file for MQTT client"

  echo "LOCATION=" > "$OPT_PATH/lib/.env"
  echo "MQTT_USER=" >> "$OPT_PATH/lib/.env"
  echo "MQTT_PASS=" >> "$OPT_PATH/lib/.env"
  echo "MQTT_HOST=" >> "$OPT_PATH/lib/.env"
fi

su monitor -c "cd '$OPT_PATH/lib' && pip3 install -r ./requirements.txt"

# permission reset

chown monitor:monitor -R /home/monitor
chown monitor:monitor -R "$OPT_PATH/lib"

# Systemd 

cp -uf "$OPT_PATH/ups-monitor.service" /etc/systemd/system/ups-monitor.service
systemctl daemon-reload
systemctl enable ups-monitor.service
systemctl restart ups-monitor.service

# Manual items to do

echo "You need to manually update the .env file with the current environment variables needed for the MQTT client"
echo "You also need to load the root CA into the systems trusted certificates"
echo "reboot the device to complete the installation"
