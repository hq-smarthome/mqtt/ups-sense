#!/usr/bin/python3


import re
import os
import paho.mqtt.client as mqtt
from subprocess import Popen, PIPE, run

from dotenv import load_dotenv

load_dotenv()

location = os.getenv("LOCATION")
mqttUser = os.getenv("MQTT_USER")
mqttPass = os.getenv("MQTT_PASS")
mqttHost = os.getenv("MQTT_HOST")

topicPrefix = "hq/"
clientId = "ups/" + location

monitorUpdateTopic = topicPrefix + clientId + "/update"

batteryTemperatureTopic = topicPrefix + clientId + "/battery/temperature"
batteryVoltageTopic = topicPrefix + clientId + "/battery/voltage"

inputVoltageTopic = topicPrefix + clientId + "/input/voltage"

outputVoltageTopic = topicPrefix + clientId + "/output/voltage"
outputFrequencyTopic = topicPrefix + clientId + "/output/frequency"

upsLoadTopic = topicPrefix + clientId + "/ups/load"

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

    client.subscribe(monitorUpdateTopic)

def on_disconnect(client, userdata,rc=0):
    logging.debug("Disconnected result code "+str(rc))

def on_message(client, userdata, message):
    if message.topic == monitorUpdateTopic:
        result = run(['upsc', 'hq-ups0'], stdout=PIPE, stderr=PIPE, text=True)

        if result.returncode == 0:
            batteryTemp = re.search(r'battery.temperature: ([\d\.]+)', result.stdout).group(1)
            client.publish(batteryTemperatureTopic, batteryTemp)

            batteryVoltage = re.search(r'battery.voltage: ([\d\.]+)', result.stdout).group(1)
            client.publish(batteryVoltageTopic, batteryVoltage)

            inputVoltage = re.search(r'input.voltage: ([\d\.]+)', result.stdout).group(1)
            client.publish(inputVoltageTopic, inputVoltage)

            outputVoltage = re.search(r'output.voltage: ([\d\.]+)', result.stdout).group(1)
            client.publish(outputVoltageTopic, outputVoltage)

            outputFreq = re.search(r'output.frequency: ([\d\.]+)', result.stdout).group(1)
            client.publish(outputFrequencyTopic, outputFreq)

            upsLoad = re.search(r'ups.load: ([\d\.]+)', result.stdout).group(1)
            client.publish(upsLoadTopic, upsLoad)

client = mqtt.Client(client_id=clientId, clean_session=True)
client.on_connect = on_connect
client.on_message = on_message

client.tls_set()
client.username_pw_set(mqttUser, password=mqttPass)
client.connect(mqttHost, 8883, 60)

client.loop_forever()
